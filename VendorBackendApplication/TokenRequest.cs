﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendorBackendApplication
{
    public class TokenRequest
    {
        public string Client_id { get; set; }
        public string Code { get; set; }
        public string Redirect_uri { get; set; }
        public string Code_Verifier { get; set; }
        public string Grant_Type { get; set; }

    }
}
