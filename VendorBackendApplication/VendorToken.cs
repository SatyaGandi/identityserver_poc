﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendorBackendApplication
{
    public class VendorToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string IdToken { get; set; }
        public VendorToken(string acessToken,string refreshToken,string idToken)
        {
            AccessToken = acessToken;
            RefreshToken = refreshToken;
            IdToken = idToken;
        }
    }
}
