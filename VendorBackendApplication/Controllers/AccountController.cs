﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace VendorBackendApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<VendorBackendController> _logger;
        public AccountController(ILogger<VendorBackendController> logger)
        {
            _logger = logger;
        }

        [HttpGet("login")]
        public async void Login()
        {
            var client = new HttpClient();

            //var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");

            //var resp = await client.GetAsync("https://localhost:5001/Account/Login?ReturnUrl=/connect/authorize/callback?client_id=js&redirect_uri=https://localhost:61202/api/account/callback&response_type=code&scope=openid profile offline_access api1&response_mode=query");
            var resp = await client.GetAsync("https://localhost:5001/Account/Login?ReturnUrl=%2Fconnect%2Fauthorize%2Fcallback%3Fclient_id%3Djs%26redirect_uri%3Dhttps%253A%252F%252Flocalhost%253A5050%252Fcallback.html%26response_type%3Dcode%26scope%3Dopenid%2520profile%2520offline_access%2520api1%26state%3Db628ce729c63451099aeaad9e718eb4b%26code_challenge%3DE6mZjFhAMCIJan-3aPA7gAk548ZZ97CBKdbicx9aUW8%26code_challenge_method%3DS256%26response_mode%3Dquery");


        }

        [HttpGet("token")]
        public async Task<TokenResponse> GetToken(string authorizationCode, string codeVerifier)
        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return null;
            }

            // request token
            //var tokenResponse = await client.RequestAuthorizationCodeTokenAsync(new AuthorizationCodeTokenRequest
            //{
            //    Address = disco.TokenEndpoint,
            //    ClientId = "js",
            //    //ClientSecret = "secret",
            //    RedirectUri = "http://localhost:61202/api/account/tokenResponse",
            //    Code = authorizationCode
            //});
            //var RedirectUri = "http://localhost:61202/api/account/callback";
            var RedirectUri = "https://localhost:5050/callback.html";

            TokenClient tc = new TokenClient(disco.TokenEndpoint, "vendorPortal", "secret", null, AuthenticationStyle.PostValues);
            var tokenResponse = await tc.RequestAuthorizationCodeAsync(authorizationCode, RedirectUri, codeVerifier);
            //var tokenResponse = await client.RequestAuthorizationCodeTokenAsync(new AuthorizationCodeTokenRequest
            //{
            //    Address = disco.TokenEndpoint,
            //    ClientId = "js",
            //    ClientSecret = "secret",
            //    ////RedirectUri = "http://localhost:61202/api/account/tokenResponse",
            //    //RedirectUri = "http://localhost:61202/api/account/callback",
            //    RedirectUri = "http:localhost:5050/callback.html",
            //    Code = authorizationCode,
            //    CodeVerifier = state,
            //    GrantType = "authorization_code"

            //});
            return tokenResponse;

        }

        [HttpPost("token")]
        //public async Task<VendorToken> GetTokenByPost(string authorizationCode, string codeVerifier)
        //public async Task<VendorToken> GetTokenByPost([FromForm]string obj)
        public async Task<TokenResult> GetTokenByPost([FromForm] TokenRequest obj)

        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return null;
            }

            var RedirectUri = "https://localhost:5050/callback.html";

            //TokenClient tc = new TokenClient(disco.TokenEndpoint, "vendorPortal", "secret", null, AuthenticationStyle.PostValues);
            TokenClient tc = new TokenClient(disco.TokenEndpoint, obj.Client_id, null, AuthenticationStyle.PostValues);
            //var tokenResponse = await tc.RequestAuthorizationCodeAsync(authorizationCode, RedirectUri, codeVerifier);
            var tokenResponse = await tc.RequestAuthorizationCodeAsync(obj.Code, RedirectUri, obj.Code_Verifier);
            //var tokenResponse = await tc.RequestAuthorizationCodeAsync(null, RedirectUri, null);

            TokenRegister.AddOrUpdateEntry(tokenResponse.IdentityToken, tokenResponse.RefreshToken);
            return new TokenResult {id_token= tokenResponse.IdentityToken,access_token=tokenResponse.AccessToken
            //, refresh_token = tokenResponse.RefreshToken
            ,expires_in=tokenResponse.ExpiresIn
            ,token_type=tokenResponse.TokenType};
            //return new VendorToken(tokenResponse.AccessToken, tokenResponse.RefreshToken, tokenResponse.IdentityToken);

        }

        [HttpGet("callback")]
        public async void callback(string code, string state)
        {
            var token = await GetToken(code, state);
        }

        [HttpGet("tokenResponse")]
        public async void tokenResponse(string code)
        {
            var token = await GetToken(code, "");
        }

        [HttpPost("TokenByRefreshToken")]
        public async Task<TokenResult> TokenByRefreshToken(string refreshToken,string idToken)
        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return null;
            }


            var tokenResponse = await client.RequestRefreshTokenAsync(new RefreshTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "vendorPortal",
                //ClientSecret = "secret",
                ////RedirectUri = "http://localhost:61202/api/account/tokenResponse",
                //RedirectUri = "http://localhost:61202/api/account/callback",
                ////RedirectUri = "http:localhost:5050/callback.html",
                GrantType = "refresh_token",
                RefreshToken = refreshToken

            });
            TokenRegister.UpdateEntry(idToken, tokenResponse.RefreshToken);

            return new TokenResult
            {
                id_token = tokenResponse.IdentityToken,
                access_token = tokenResponse.AccessToken
           //, refresh_token = tokenResponse.RefreshToken
           ,
                expires_in = tokenResponse.ExpiresIn
           ,
                token_type = tokenResponse.TokenType
            };

            

        }

        //[HttpGet("TokenByCode")]
        //public async Task<TokenResponse> TokenByCode(string authCode)
        //{
        //    var client = new HttpClient();

        //    var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
        //    if (disco.IsError)
        //    {
        //        Console.WriteLine(disco.Error);
        //        return null;
        //    }


        //    var tokenResponse = await client.RequestAuthorizationCodeTokenAsync(new RefreshTokenRequest
        //    {
        //        Address = disco.AuthorizeEndpoint,
        //        ClientId = "js",
        //        ClientSecret = "secret",
        //        ////RedirectUri = "http://localhost:61202/api/account/tokenResponse",
        //        //RedirectUri = "http://localhost:61202/api/account/callback",
        //        ////RedirectUri = "http:localhost:5050/callback.html",
        //        GrantType = "refresh_token",
        //        RefreshToken = refreshToken

        //    });
        //    return tokenResponse;

        //}

        [HttpPost("RefreshTokenByIdToken")]
        public async Task<string> RefreshTokenByIdToken(string idToken)
        {
            var tokenResponse = TokenRegister.GetRefreshToken(idToken);
            return tokenResponse;

        }
    }
}
