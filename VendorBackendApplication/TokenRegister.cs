﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;

namespace VendorBackendApplication
{
    public static class TokenRegister
    {
        private static Dictionary<string, string> tokenEntries;

        static JwtSecurityTokenHandler handler;


        static TokenRegister()
        {
            tokenEntries = new Dictionary<string, string>();
            handler = new JwtSecurityTokenHandler();
        }
        private static string Subject(this string idToken)
        {
            var token = handler.ReadJwtToken(idToken);
            return token.Subject;
        }
        public static void AddOrUpdateEntry(string idToken, string refreshToken)
        {
            var subject = idToken.Subject();
            if (tokenEntries.ContainsKey(subject))
            {
                tokenEntries[subject] = refreshToken;
            }
            else
            {
                tokenEntries.Add(subject, refreshToken);
            }
        }
        public static string GetRefreshToken(string idToken)
        {
            var refreshToken = "";
            try
            {
                refreshToken = tokenEntries[idToken.Subject()];
            }
            catch (Exception ex)
            {
                throw new Exception("No id token found");
            }

            return refreshToken;
        }

        internal static void UpdateEntry(string idToken, string refreshToken)
        {
            tokenEntries[idToken.Subject()] = refreshToken;
        }
    }
}
