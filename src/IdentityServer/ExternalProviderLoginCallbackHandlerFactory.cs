﻿//using Microsoft.Extensions.DependencyInjection;
//using System;


//namespace IdentityServer
//{
//    public class ExternalProviderLoginCallbackHandlerFactory
//    {
//        private readonly IServiceProvider _serviceProvider;

//        public ExternalProviderLoginCallbackHandlerFactory(IServiceProvider serviceProvider)
//        {
//            _serviceProvider = serviceProvider;
//        }

//        public IExternalProviderLoginCallbackHandler Create(string externalProvider)
//        {
//            switch (externalProvider.ToLowerInvariant())
//            {
//                //case "google":
//                //    return _serviceProvider.GetRequiredService<GoogleLoginCallbackHandler>();
//                //case "linkedin":
//                //    return _serviceProvider.GetRequiredService<LinkedInLoginCallbackHandler>();
//                case "visionplannerb2c":
//                    return _serviceProvider.GetRequiredService<B2cLoginCallbackHandler>();
//                default:
//                    throw new Exception($"External provider login callback for {externalProvider} not registered");
//            }
//        }
//    }
//}
