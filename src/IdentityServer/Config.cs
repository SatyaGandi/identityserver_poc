// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };


        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("api1", "My API")
            };
        // scopes define the API resources in your system
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
    {
        new ApiResource("api1", "My API",
                new[] { "CustomKey1","CustomKey2"})
    };
        }
        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                // machine to machine client
                new Client
                {
                    ClientId = "client",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    // scopes that client has access to
                    AllowedScopes = { "api1"}
                },
                
                // interactive ASP.NET Core MVC client
                new Client
                {
                    ClientId = "mvc",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,
                    
                    // where to redirect to after login
                    RedirectUris = { "https://localhost:5002/signin-oidc" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "https://localhost:5002/signout-callback-oidc" },
                    AllowOfflineAccess = true,

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        IdentityServerConstants.StandardScopes.Profile,
                         "api1"
                    }
                },
                new Client
                {
                    ClientId = "vendorPortal",
                    //ClientSecrets = { new Secret("secret".Sha256()) },
                    ClientName="vendor Portal",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    
                    // where to redirect to after login
                    RedirectUris = { "https://localhost:61202/api/account/callback","https://localhost:5050/callback.html" },

                    // where to redirect to after logout
                    //PostLogoutRedirectUris = { "https://localhost:61202/signout-callback-oidc" },
                    PostLogoutRedirectUris = { "https://localhost:5050/index.html" },
                    AllowOfflineAccess = true,

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        IdentityServerConstants.StandardScopes.Profile,
                         "api1"

                    },
                    AuthorizationCodeLifetime = 3600
                },
                // JavaScript Client
                new Client
                {
                    ClientId = "jsClient",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,

                    RedirectUris =           { "https://localhost:5003/callback.html" },
                    PostLogoutRedirectUris = { "https://localhost:5003/index.html" },
                    AllowedCorsOrigins =     { "https://localhost:5003" },
                    AllowOfflineAccess = true,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                         IdentityServerConstants.StandardScopes.OfflineAccess,
                        "api1"
                    }
                }
            };
        //new Client
        //   {
        //       ClientId = "js",
        //       ClientName = "JavaScript Client",
        //       AllowedGrantTypes = GrantTypes.Code,
        //       RequireClientSecret = true,
        //       ClientSecrets={ new Secret("secret".Sha256()) },
        //       RequirePkce = false,

        //       //RedirectUris =           { "https://localhost:5050/callback.html" },
        //       RedirectUris =           { "http://localhost:61202/api/account/callback" },
        //       PostLogoutRedirectUris = { "https://localhost:5050/index.html" },
        //       AllowedCorsOrigins =     { "https://localhost:5050","https://localhost:61202" },
        //       //AllowedCorsOrigins =     { "*" },
        //       AllowOfflineAccess = true,
        //       AllowedScopes =
        //       {
        //           IdentityServerConstants.StandardScopes.OpenId,
        //           IdentityServerConstants.StandardScopes.Profile,
        //            IdentityServerConstants.StandardScopes.OfflineAccess,
        //           "api1"
        //       }
        //   }
    //};
    }
}