﻿using IdentityServer4;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System;
using Microsoft.AspNetCore.Mvc.Routing;

namespace IdentityServer
{
    public static class B2CAuthenticationBuilderExtension
    {
        static B2CAuthenticationBuilderExtension()
        {
            //userFlows.Add("SignIn", "B2C_1_VPC_Ids_Sign_In");
            //userFlows.Add("SignUp", "B2C_1_VPC_Ids_Sign_Up");

            userFlows.Add("SignIn", "B2C_1_SignIn_Preview_0.1");
            userFlows.Add("SignUp", "B2C_1_SignUp_Preview_0.1");
            //userFlows.Add("ResetPassword", "B2C_1_VPC_Ids_Reset_Password");

            // https://docs.microsoft.com/en-us/azure/active-directory-b2c/error-codes
            remoteFailures.Add("reset_password_requested", "AADB2C90118");
            remoteFailures.Add("reset_password_cancelled", "AADB2C90091");
        }

        private static Dictionary<string, string> userFlows = new Dictionary<string, string>();

        private static Dictionary<string, string> remoteFailures = new Dictionary<string, string>();

        private static void SetB2COptions(OpenIdConnectOptions options, IConfiguration configuration, string metadataAddress)
        {
            options.MetadataAddress = metadataAddress;
            options.UsePkce = true;
            options.ResponseType = OpenIdConnectResponseType.CodeIdToken;
            options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
            options.ClientId = configuration["IdentityServer:B2C:ClientID"];
            options.ClientSecret = configuration["IdentityServer:B2C:ClientSecret"];
            options.Scope.Add(options.ClientId);

            options.Events.OnRedirectToIdentityProvider = (RedirectContext context) =>
            {
                if (context.Properties.Items.ContainsKey("domainHint"))
                {
                    context.ProtocolMessage.DomainHint = context.Properties.Items["domainHint"];
                }

                if (context.Properties.Items.ContainsKey("loginHint"))
                {
                    context.ProtocolMessage.LoginHint = context.Properties.Items["loginHint"];
                }

                if (context.Properties.Items.TryGetValue("prompt", out string prompt))
                {
                    if (!string.IsNullOrEmpty(prompt))
                    {
                        context.ProtocolMessage.Prompt = prompt;
                    }
                }

                return Task.CompletedTask;
            };

            options.Events.OnRemoteFailure = (RemoteFailureContext context) =>
            {
                // Handle the error that is raised when a user has requested to recover a password.
                if (!string.IsNullOrEmpty(context.Failure.Message) && context.Failure.Message.Contains(remoteFailures["reset_password_requested"]))
                {
                    context.Response.Redirect($"External/Challenge?provider=VisionplannerB2C&userFlow=ResetPassword&returnUrl={HttpUtility.UrlEncode(context.Properties.Items["returnUrl"])}");
                    context.HandleResponse();
                }
                else
                {
                    // Get the base URL from the redirect_uri query string parameter in the returnUrl
                    var returnUri = new Uri($"{context.Request.Scheme}://{context.Request.Host}{context.Properties.Items["returnUrl"]}");
                    var queryString = returnUri.Query;
                    var queryDictionary = HttpUtility.ParseQueryString(queryString);
                    var redirectUriFromQueryString = new Uri(queryDictionary["redirect_uri"]);
                    var redirectUrl = new UriBuilder(redirectUriFromQueryString.Scheme, redirectUriFromQueryString.Host, redirectUriFromQueryString.Port, "", "");
                    var message = "Het aanmelden is afgebroken";

                    context.Response.Redirect($"Account/AuthenticationError?redirectUrl={redirectUrl}&authenticationErrorMessage={message}");
                    context.HandleResponse();
                }

                return Task.CompletedTask;
            };
        }

        public static AuthenticationBuilder AddVisionplannerB2C(this AuthenticationBuilder authenticationBuilder)
        {
            return authenticationBuilder
            .AddPolicyScheme("VisionplannerB2C", "VisionplannerB2C", options =>
            {
                options.ForwardDefaultSelector = context => {
                    var userFlow = context.Request.Query.ToList().Find(parameter => parameter.Key.Equals("userFlow")).Value;
                    Console.WriteLine(context.Request.Query.ToList());

                    try
                    {
                        var scheme = userFlows[userFlow];
                        return scheme;
                    }
                    catch
                    {
                        return userFlows["SignIn"];
                    }
                };
            });
        }

        public static AuthenticationBuilder AddVisionplannerB2CSignIn(this AuthenticationBuilder authenticationBuilder, IConfiguration configuration)
        {
            return authenticationBuilder
            .AddOpenIdConnect(userFlows["SignIn"], options =>
            {
                var metaDataAddress = configuration["IdentityServer:B2C:MetadataAddress"].Replace("{Userflow}", configuration["IdentityServer:B2C:Userflows:SignIn"]);
                SetB2COptions(options, configuration, metaDataAddress);
            });
        }


        public static AuthenticationBuilder AddVisionplannerB2CSignUp(this AuthenticationBuilder authenticationBuilder, IConfiguration configuration)
        {
            return authenticationBuilder
            .AddOpenIdConnect(userFlows["SignUp"], options =>
            {
                var metaDataAddress = configuration["IdentityServer:B2C:MetadataAddress"].Replace("{Userflow}", configuration["IdentityServer:B2C:Userflows:SignUp"]);
                options.CallbackPath = "/signup-oidc";
                SetB2COptions(options, configuration, metaDataAddress);
            });
        }

        public static AuthenticationBuilder AddVisionplannerB2CResetPassword(this AuthenticationBuilder authenticationBuilder, IConfiguration configuration)
        {
            return authenticationBuilder
            .AddOpenIdConnect(userFlows["ResetPassword"], options =>
            {
                var metaDataAddress = configuration["IdentityServer:B2C:MetadataAddress"].Replace("{Userflow}", configuration["IdentityServer:B2C:Userflows:ResetPassword"]);
                options.CallbackPath = "/reset-password-oidc";
                SetB2COptions(options, configuration, metaDataAddress);
            });
        }
    }

}
