﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;

namespace IdentityServer
{
    public interface IExternalProviderLoginCallbackHandler
    {
        Claim FindUserIdClaim(ClaimsPrincipal externalUser);
        string FindUserId(Claim userIdentityClaim);
    }
}
