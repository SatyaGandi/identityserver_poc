﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityModel.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    public class Program
    {
        //private static async Task Main()
        //{
        //    // discover endpoints from metadata
        //    var client = new HttpClient();

        //    var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
        //    if (disco.IsError)
        //    {
        //        Console.WriteLine(disco.Error);
        //        return;
        //    }

        //    // request token
        //    var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
        //    {
        //        Address = disco.TokenEndpoint,
        //        ClientId = "client",
        //        ClientSecret = "secret",

        //        Scope = "api1"
        //    });

        //    if (tokenResponse.IsError)
        //    {
        //        Console.WriteLine(tokenResponse.Error);
        //        return;
        //    }

        //    Console.WriteLine(tokenResponse.Json);
        //    Console.WriteLine("\n\n");

        //    // call api
        //    var apiClient = new HttpClient();
        //    apiClient.SetBearerToken(tokenResponse.AccessToken);

        //    var response = await apiClient.GetAsync("https://localhost:6001/identity");
        //    if (!response.IsSuccessStatusCode)
        //    {
        //        Console.WriteLine(response.StatusCode);
        //    }
        //    else
        //    {
        //        var content = await response.Content.ReadAsStringAsync();
        //        Console.WriteLine(JArray.Parse(content));
        //    }
        //}

        //private static async Task Main()
        //{
        //    Task.Run(async () =>
        //    {
        //        await MyAmazingMethodAsync();
        //    }).Wait();

        //    Console.ReadLine();
        //}

        private static async Task Main()
        {
            var excep = ExceptionHelper.Exceptions();

            foreach(var ex in excep)
            {
                if(ex is ExceptionA)
                {
                    Console.WriteLine(ex.Message);
                }
                if (ex is ExceptionB)
                {
                    Console.WriteLine(ex.Message);
                }
                if (ex is Exception)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.ReadLine();
        }

        public class CustomException : Exception
        {
            public CustomException(String message) : base(message)
            { }
        }

        static int WaitAndThrow(int id, int waitInMs)
        {
            Console.WriteLine($"{DateTime.UtcNow}: Task {id} started");

            Thread.Sleep(waitInMs);
            try

            {
                if (id == 2)
                {
                    //return Task.FromResult(2);
                    return 2;
                }
                throw new CustomException($"Task {id} throwing at {DateTime.UtcNow}");
            }
            catch (Exception)
            {
                Console.WriteLine($"WaitAndThrow {id}");
            }
            return 111;

        }
        static async Task MyAmazingMethodAsync()
        {
            try
            {
                Task<int>[] taskArray = { Task.Factory.StartNew(() => WaitAndThrow(1, 1000)),
                                 Task.Factory.StartNew(() => WaitAndThrow(2, 2000)),
                                 Task.Factory.StartNew(() => WaitAndThrow(3, 3000)) };

                //var resp = Task.WaitAll(taskArray);
                var resp = await Task.WhenAll(taskArray);
                Console.WriteLine("This isn't going to happen");
            }
            catch (AggregateException ex)
            {
                foreach (var inner in ex.InnerExceptions)
                {
                    Console.WriteLine($"Caught AggregateException in Main at {DateTime.UtcNow}: " + inner.Message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Caught Exception in Main at {DateTime.UtcNow}: " + ex.Message);
            }
            Console.WriteLine("Done.");
            Console.ReadLine();
        }
    }


    public static class ExceptionHelper
    {
        public static List<Exception> Exceptions()
        {
            return new List<Exception>()
            {
                new ExceptionA("ExceptionA"),
                new ExceptionB("ExceptionB"),
                new Exception("Exception")

            };
        }
    }

    public class ExceptionRepo
    {
        public Exception ExceptionInternal { get; set; }
    }
    public class ExceptionA : Exception
    {
        public ExceptionA(String message) : base(message)
        { }
    }
    public class ExceptionB : Exception
    {
        public ExceptionB(String message) : base(message)
        { }
    }

}