﻿using Microsoft.AspNetCore.Builder;

namespace VPCApplication
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
