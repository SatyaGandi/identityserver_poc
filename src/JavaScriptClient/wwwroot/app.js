/// <reference path="oidc-client.js" />

function log() {
    document.getElementById('results').innerText = '';

    Array.prototype.forEach.call(arguments, function (msg) {
        if (msg instanceof Error) {
            msg = "Error: " + msg.message;
        }
        else if (typeof msg !== 'string') {
            msg = JSON.stringify(msg, null, 2);
        }
        document.getElementById('results').innerText += msg + '\r\n';
    });
}

document.getElementById("login").addEventListener("click", login, false);
document.getElementById("api").addEventListener("click", api, false);
document.getElementById("logout").addEventListener("click", logout, false);
document.getElementById("GenerateRefreshToken").addEventListener("click", refresh, false);
//document.getElementById("redirectToApp").addEventListener("click", redirectToApp, false);

var config = {
    authority: "https://localhost:5001",
    client_id: "jsClient",
    redirect_uri: "https://localhost:5003/callback.html",
    response_type: "code",
    scope:"openid profile offline_access api1",
    //scope:"openid profile api1",
    post_logout_redirect_uri : "https://localhost:5003/index.html",
};
var mgr = new Oidc.UserManager(config);

mgr.getUser().then(function (user) {
    if (user) {
        log("User logged in", user.profile);
    }
    else {
        log("User not logged in");
    }
});

function login() {
    mgr.signinRedirect();
}

function redirectToApp() {
    //window.location.href = "https://localhost:5050";
    window.open("https://localhost:5050", "_blank");
    //mgr.signinRedirect();
}

function api() {
    mgr.getUser().then(function (user) {
        var url = "http://localhost:5000/identity";
        //var url = "http://localhost:6001/identity";


        var xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.onload = function () {
            log(xhr.status, JSON.parse(xhr.responseText));
        }
        console.log(user);
        xhr.setRequestHeader("Authorization", "Bearer " + user.access_token);
        //xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:5003');
        //xhr.append('Access-Control-Allow-Credentials', 'true');
        xhr.send();
    });
    mgr.exchangeRefreshToken().then(function (user) {
        console.debug(user);
        console.log(user);
    });
    
}

function logout() {
    mgr.signoutRedirect();
}

function refresh() {
    mgr.signinSilent().then(function (user) {
        console.debug(user);
        console.log(user);
    });

}