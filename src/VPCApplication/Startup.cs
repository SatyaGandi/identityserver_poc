﻿using Microsoft.AspNetCore.Builder;

namespace VendorPortal
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
