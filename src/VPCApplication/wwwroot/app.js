/// <reference path="oidc-client.js" />

function log() {
    document.getElementById('results').innerText = '';

    Array.prototype.forEach.call(arguments, function (msg) {
        if (msg instanceof Error) {
            msg = "Error: " + msg.message;
        }
        else if (typeof msg !== 'string') {
            msg = JSON.stringify(msg, null, 2);
        }
        document.getElementById('results').innerText += msg + '\r\n';
    });
}

document.getElementById("login").addEventListener("click", login, false);
document.getElementById("api").addEventListener("click", api, false);
document.getElementById("logout").addEventListener("click", logout, false);
//document.getElementById("GenerateRefreshToken").addEventListener("click", refresh, false);
document.getElementById("tokenFromRefreshToken").addEventListener("click", tokenFromRefreshToken, false);
//document.getElementById("vendorBackend").addEventListener("click", goToBackend, false);
//document.getElementById("generateToken").addEventListener("click", GetToken, false);
document.getElementById("getRefreshToken").addEventListener("click", getRefreshToken, false);
//document.getElementById("redirectToFrontEnd").addEventListener("click", redirectToFrontEnd, false);
//document.getElementById("redirectToApp").addEventListener("click", login, false);



var config = {
    authority: "https://localhost:5001",
    client_id: "vendorPortal",
    //client_secret: "secret",
    redirect_uri: "https://localhost:5050/callback.html",
    //redirect_uri: "http://localhost:61202/api/account/callback",
    response_type: "code",
    scope: "openid profile offline_access api1",
    //scope: "openid profile api1",

    post_logout_redirect_uri: "https://localhost:5050/index.html",

};
var mgr = new Oidc.UserManager(config);

mgr.getUser().then(function (user) {
    if (user) {
        log("User logged in", user.profile);
    }
    else {
        log("User not logged in");
    }
});

function login() {
    mgr.signinRedirect();
}

function api() {
    mgr.getUser().then(function (user) {
        var url = "http://localhost:5000/identity";
        //var url = "http://localhost:6001/identity";


        var xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.onload = function () {
            log(xhr.status, JSON.parse(xhr.responseText));
        }
        console.log(user);
        xhr.setRequestHeader("Authorization", "Bearer " + user.access_token);
        //xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:5003');
        //xhr.append('Access-Control-Allow-Credentials', 'true');
        xhr.send();
    });
    //mgr.exchangeRefreshToken().then(function (user) {
    //    console.debug(user);
    //    console.log(user);
    //});

}

function logout() {
    mgr.signoutRedirect();
}

function refresh() {
    mgr.signinSilent().then(function (user) {
        console.debug(user);
        console.log(user);
    });

}

function goToBackend() {
    var url = "https://localhost:61202/api/account/TokenByRefreshToken?refreshToken";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = function () {
        log(xhr.status, JSON.parse(xhr.responseText));
    }
    console.log(user);
    xhr.setRequestHeader("Authorization", "Bearer " + user.access_token);
    //xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:5003');
    //xhr.append('Access-Control-Allow-Credentials', 'true');
    xhr.send();
}

//function GetToken() {
//    console.debug();
//    var refreshToken = document.getElementById("refreshTokenText").value ;
//    var url = "https://localhost:5001/connect/token";
//    var data = new FormData();
//    //data.append('request_type', 'si:s');
//    //data.append('refresh_token', refreshToken);
//    //data.append('grant_type', 'refresh_token');
//    //data.append('client_id', 'js');
//    //data.append('client_secret', 'secret');

//    data.append('request_type', 'si:s');
//    data.append('client_id', 'vendorPortal');
//    data.append('client_secret', 'secret');
//    var xhr = new XMLHttpRequest();
//    xhr.open('POST', url, true);
//    xhr.onload = function () {
//        // do something to response
//        console.log(this.responseText);
//    };
//    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
//    xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
//    //xhr.setRequestHeader('content-type', 'application/json');


//    xhr.send(data);
//}
function GetToken() {
    var refreshToken = document.getElementById("refreshTokenText").value;
    var codeVerifier = document.getElementById("codeVerifierText").value;
    //var url = "http://localhost:61202/api/account/TokenByRefreshToken?refreshToken=" + refreshToken
    //    +"&codeVerifer="+codeVerifier;
    var url = "http://localhost:61202/api/account/token?authorizationCode=" + refreshToken
        + "&codeVerifer=" + codeVerifier;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = function () {
        log(xhr.status, JSON.parse(xhr.responseText));
    }
    xhr.send();
}

function redirectToFrontEnd() {
    var url = "http://localhost:61202/api/vendorBackend/GoToBrowser";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = function () {
        log(xhr.status, JSON.parse(xhr.responseText));
    }
    //xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:61202');
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.send();
}
function getRefreshToken() {
    document.getElementById("RefreshTokenValue").innerHTML = "";
    let sessionDetails = JSON.parse(sessionStorage["oidc.user:https://localhost:5001:vendorPortal"]);
    let idToken = sessionDetails.id_token;
    var url = "https://localhost:61202/api/account/RefreshTokenByIdToken?idToken=" + idToken;
    var xhr = new XMLHttpRequest();
    xhr.open("Post", url);
    var refreshToken = "";
    xhr.onload = function () {
        console.log(xhr.status, xhr.responseText);
        refreshToken = xhr.responseText;
        document.getElementById("RefreshTokenValue").innerHTML = refreshToken;

    }
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.send();

}
function tokenFromRefreshToken() {
    document.getElementById("TokenValue").innerHTML = "";
    var refreshToken = document.getElementById("RefreshTokenValue").innerHTML;
    let sessionDetails = JSON.parse(sessionStorage["oidc.user:https://localhost:5001:vendorPortal"]);
    let idToken = sessionDetails.id_token;

    var url = "https://localhost:61202/api/account/TokenByRefreshToken?refreshToken=" + refreshToken + "&idToken=" + idToken;
    var xhr = new XMLHttpRequest();
    xhr.open("Post", url);
    var accessToken = "";
    xhr.onload = function () {
        console.log(xhr.status, xhr.responseText);
        accessToken = xhr.responseText;
        document.getElementById("TokenValue").innerHTML = accessToken;

    }
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.send();
}